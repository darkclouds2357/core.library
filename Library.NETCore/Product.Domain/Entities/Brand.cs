﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Product.Domain.Entities
{
    public class Brand : BaseEntity
    {
        public Brand()
        {
            Products = new HashSet<Product>();
        }

        public string BrandName { get; set; }
        public string Description { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
