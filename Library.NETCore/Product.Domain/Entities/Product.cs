﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;

namespace Product.Domain.Entities
{
    public class Product : BaseEntity, ISecuredCondition<Product>
    {
        public Product()
        {
            ProductHistories = new HashSet<ProductHistory>();
        }

        public string Code { get; set; }

        public string Name { get; set; }

        public int NotInCartQuantity { get; set; }

        public int ActualQuantity { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string PictureUri { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public Guid SubCategoryId { get; set; }
        public SubCategory SubCategory { get; set; }

        public Guid BrandId { get; set; }

        public Brand Brand { get; set; }

        public ICollection<ProductHistory> ProductHistories { get; set; }

        public Expression<Func<Product, bool>> SecuredCondition =>
            product =>
            (product.Category != null && !product.Category.IsDeleted) &&
            (product.SubCategory != null && !product.SubCategory.IsDeleted) &&
            (product.Brand != null && !product.Brand.IsDeleted);
    }
}
