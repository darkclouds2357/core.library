﻿using Product.Domain.Enum;
using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Product.Domain.Entities
{
    /// <summary>
    /// For Log history of Product, dont need to write CreatedBy/UpdatedBy/UpdatedOn
    /// ActionBy is note from system
    /// This is apply with PermissionRuleEntity: only for View and Insert (cant delete or update)
    /// </summary>
    public class ProductHistory : BaseEntity
    {
        public string Description { get; set; }

        public ProductChangedAction Action { get; set; }

        public decimal ChangedValue { get; set; }

        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        public string ActionBy { get; set; }

        public Guid? CorrelationId { get; set; }
        [NotMapped]
        public override Guid? CreatedBy { get; set; }
        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
        [NotMapped]
        public override bool IsDeleted { get; set; }
    }
}
