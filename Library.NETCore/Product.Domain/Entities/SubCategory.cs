﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;

namespace Product.Domain.Entities
{
    public class SubCategory : BaseEntity, ISecuredCondition<SubCategory>
    {
        public SubCategory()
        {
            Products = new HashSet<Product>();
        }
        public string SubCategoryName { get; set; }
        public string Description { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<Product> Products { get; set; }

        public Expression<Func<SubCategory, bool>> SecuredCondition => subCat => subCat.Category != null && !subCat.Category.IsDeleted;
    }
}
