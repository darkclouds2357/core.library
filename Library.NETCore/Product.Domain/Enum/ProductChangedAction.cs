﻿using System;

namespace Product.Domain.Enum
{
    public enum ProductChangedAction
    {
        OrderedProduct = 1,
        AddedProductQuantity = 2,
        AddedProductToCart = 3,
        RemoveProductFromCart = 4,
        MinusPrice = 5,
        PlusPrice = 6,
        Delete = 7,
    }
}
