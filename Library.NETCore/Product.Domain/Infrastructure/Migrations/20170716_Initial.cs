﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;
using Product.Domain.Entities;
using Product.Domain.Infrastructure.Migrations;

namespace Product.Domain.Migrations
{
    public class Initial : Migration, IProductMigration
    {
        private const string PRODUCT_TABLES_SCHEMA = "prd";
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            ProductUp(migrationBuilder);
            BrandUp(migrationBuilder);
            CategoryUp(migrationBuilder);
            SubCategoryUp(migrationBuilder);
            ProductHistoryUp(migrationBuilder);
        }

        private void ProductHistoryUp(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(ProductHistory),
                schema: PRODUCT_TABLES_SCHEMA,
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(ProductHistory.Id)),
                    CreatedOn = table.Column<DateTime>(nullable: true, name: nameof(ProductHistory.CreatedOn)),
                    Description = table.Column<string>(nullable: true, maxLength: int.MaxValue, name: nameof(ProductHistory.Description)),

                    Action = table.Column<int>(nullable: false, name: nameof(ProductHistory.Action)),
                    ActionBy = table.Column<string>(nullable: false, name: nameof(ProductHistory.ActionBy)),
                    ChangedValue = table.Column<decimal>(nullable: false, name: nameof(ProductHistory.ChangedValue)),
                    ProductId = table.Column<Guid>(nullable: false, name: nameof(ProductHistory.ProductId)),
                    CorrelationId = table.Column<Guid>(nullable: true, name: nameof(ProductHistory.CorrelationId))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductHistory_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: nameof(Entities.Product),
                        principalColumn: nameof(Entities.Product.Id),
                        principalSchema: PRODUCT_TABLES_SCHEMA);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_ProductId",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(ProductHistory),
                column: nameof(ProductHistory.ProductId));
            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_CorrelationId",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(ProductHistory),
                column: nameof(ProductHistory.CorrelationId));
            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_Action",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(ProductHistory),
                column: nameof(ProductHistory.Action));
        }

        private void SubCategoryUp(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(SubCategory),
                schema: PRODUCT_TABLES_SCHEMA,
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(SubCategory.Id)),
                    CreatedOn = table.Column<DateTime>(nullable: true, name: nameof(SubCategory.CreatedOn)),
                    CreatedBy = table.Column<Guid>(nullable: true, name: nameof(SubCategory.CreatedBy)),
                    UpdatedOn = table.Column<DateTime>(nullable: true, name: nameof(SubCategory.UpdatedOn)),
                    UpdatedBy = table.Column<Guid>(nullable: true, name: nameof(SubCategory.UpdatedBy)),
                    IsDeleted = table.Column<bool>(nullable: false, name: nameof(SubCategory.IsDeleted)),

                    SubCategoryName = table.Column<string>(nullable: false, maxLength: 100, name: nameof(SubCategory.SubCategoryName)),
                    Description = table.Column<string>(nullable: true, maxLength: int.MaxValue, name: nameof(SubCategory.Description)),
                    CategoryId = table.Column<Guid>(nullable: false, name: nameof(SubCategory.CategoryId))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: nameof(Category),
                        principalColumn: nameof(Category.Id),
                        principalSchema: PRODUCT_TABLES_SCHEMA);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubCategory_SubCategoryName",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(SubCategory),
                column: nameof(SubCategory.SubCategoryName));
            migrationBuilder.CreateIndex(
                name: "IX_SubCategory_CategoryID",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(SubCategory),
                column: nameof(SubCategory.CategoryId));
        }

        private void CategoryUp(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(Category),
                schema: PRODUCT_TABLES_SCHEMA,
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(Category.Id)),
                    CreatedOn = table.Column<DateTime>(nullable: true, name: nameof(Category.CreatedOn)),
                    CreatedBy = table.Column<Guid>(nullable: true, name: nameof(Category.CreatedBy)),
                    UpdatedOn = table.Column<DateTime>(nullable: true, name: nameof(Category.UpdatedOn)),
                    UpdatedBy = table.Column<Guid>(nullable: true, name: nameof(Category.UpdatedBy)),
                    IsDeleted = table.Column<bool>(nullable: false, name: nameof(Category.IsDeleted)),

                    CategoryName = table.Column<string>(nullable: false, maxLength: 100, name: nameof(Category.CategoryName)),
                    Description = table.Column<string>(nullable: true, maxLength: int.MaxValue, name: nameof(Category.Description))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Category_CategoryName",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Category),
                column: nameof(Category.CategoryName));
        }

        private void BrandUp(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(Brand),
                schema: PRODUCT_TABLES_SCHEMA,
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(Brand.Id)),
                    CreatedOn = table.Column<DateTime>(nullable: true, name: nameof(Brand.CreatedOn)),
                    CreatedBy = table.Column<Guid>(nullable: true, name: nameof(Brand.CreatedBy)),
                    UpdatedOn = table.Column<DateTime>(nullable: true, name: nameof(Brand.UpdatedOn)),
                    UpdatedBy = table.Column<Guid>(nullable: true, name: nameof(Brand.UpdatedBy)),
                    IsDeleted = table.Column<bool>(nullable: false, name: nameof(Brand.IsDeleted)),

                    BrandName = table.Column<string>(nullable: false, maxLength: 100, name: nameof(Brand.BrandName)),
                    Description = table.Column<string>(nullable: true, maxLength: int.MaxValue, name: nameof(Brand.Description))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brand", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Brand_BrandName",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Brand),
                column: nameof(Brand.BrandName));
        }

        private void ProductUp(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(Entities.Product),
                schema: PRODUCT_TABLES_SCHEMA,
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(Entities.Product.Id)),
                    CreatedOn = table.Column<DateTime>(nullable: true, name: nameof(Entities.Product.CreatedOn)),
                    CreatedBy = table.Column<Guid>(nullable: true, name: nameof(Entities.Product.CreatedBy)),
                    UpdatedOn = table.Column<DateTime>(nullable: true, name: nameof(Entities.Product.UpdatedOn)),
                    UpdatedBy = table.Column<Guid>(nullable: true, name: nameof(Entities.Product.UpdatedBy)),
                    IsDeleted = table.Column<bool>(nullable: false, name: nameof(Entities.Product.IsDeleted)),

                    Name = table.Column<string>(nullable: false, maxLength: 100, name: nameof(Entities.Product.Name)),
                    Description = table.Column<string>(nullable: true, maxLength: int.MaxValue, name: nameof(Entities.Product.Description)),
                    Code = table.Column<string>(nullable: false, maxLength: 100, name: nameof(Entities.Product.Code)),

                    NotInCartQuantity = table.Column<int>(nullable: false, name: nameof(Entities.Product.NotInCartQuantity)),
                    ActualQuantity = table.Column<int>(nullable: false, name: nameof(Entities.Product.ActualQuantity)),
                    Price = table.Column<decimal>(nullable: false, name: nameof(Entities.Product.Price)),
                    PictureUri = table.Column<string>(nullable: true, name: nameof(Entities.Product.PictureUri)),

                    CategoryId = table.Column<Guid>(nullable: false, name: nameof(Entities.Product.CategoryId)),
                    SubCategoryId = table.Column<Guid>(nullable: false, name: nameof(Entities.Product.SubCategoryId)),
                    BrandId = table.Column<Guid>(nullable: false, name: nameof(Entities.Product.BrandId)),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: nameof(Category),
                        principalColumn: nameof(Category.Id),
                        principalSchema: PRODUCT_TABLES_SCHEMA);
                    table.ForeignKey(
                        name: "FK_Product_SubCategory_SubCategoryId",
                        column: x => x.SubCategoryId,
                        principalTable: nameof(SubCategory),
                        principalColumn: nameof(SubCategory.Id),
                        principalSchema: PRODUCT_TABLES_SCHEMA);
                    table.ForeignKey(
                        name: "FK_Product_Brand_BrandId",
                        column: x => x.BrandId,
                        principalTable: nameof(Brand),
                        principalColumn: nameof(Brand.Id),
                        principalSchema: PRODUCT_TABLES_SCHEMA);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_Code",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Entities.Product),
                column: nameof(Entities.Product.Code),
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryID",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Entities.Product),
                column: nameof(Entities.Product.CategoryId)
                );
            migrationBuilder.CreateIndex(
                name: "IX_Product_SubCategoryID",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Entities.Product),
                column: nameof(Entities.Product.SubCategoryId)
                );
            migrationBuilder.CreateIndex(
                name: "IX_Product_BrandID",
                schema: PRODUCT_TABLES_SCHEMA,
                table: nameof(Entities.Product),
                column: nameof(Entities.Product.BrandId)
                );
        }
    }
}
