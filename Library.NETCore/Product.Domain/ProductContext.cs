﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Entities;

namespace Product.Domain
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
        }

        public DbSet<Entities.Product> Products { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductHistory> ProductHistories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entities.Product>(ProductConfigure);
            modelBuilder.Entity<Brand>(BrandConfigure);
            modelBuilder.Entity<Category>(CategoryConfigure);
            modelBuilder.Entity<SubCategory>(SubCategoryConfigure);
            modelBuilder.Entity<ProductHistory>(ProductHistoryConfigure);
        }

        private void ProductHistoryConfigure(EntityTypeBuilder<ProductHistory> builder)
        {
            builder.ToTable(nameof(ProductHistory));
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).IsRequired();

            builder.Property(p => p.Action).IsRequired();
            builder.Property(p => p.ActionBy).IsRequired();
            builder.HasIndex(p => p.Action).IsUnique(false);

            builder.Property(p => p.CorrelationId).IsRequired(false);
            builder.HasIndex(p => p.CorrelationId).IsUnique(false);

            builder.Property(p => p.ChangedValue).IsRequired();
            builder.Property(p => p.Description).IsRequired();

            builder.HasOne(p => p.Product).WithMany().HasForeignKey(p => p.ProductId);
            builder.HasIndex(p => p.ProductId).IsUnique(false);
        }

        private void SubCategoryConfigure(EntityTypeBuilder<SubCategory> builder)
        {
            builder.ToTable(nameof(SubCategory));
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Id).IsRequired();
            builder.Property(s => s.IsDeleted).IsRequired();
            builder.Property(s => s.SubCategoryName).IsRequired().HasMaxLength(100);
            builder.HasIndex(s => s.SubCategoryName).IsUnique(false);

            builder.Property(s => s.Description).HasMaxLength(int.MaxValue);

            builder.HasOne(s => s.Category).WithMany().HasForeignKey(s => s.CategoryId);
            builder.HasIndex(s => s.CategoryId).IsUnique(false);
        }

        private void CategoryConfigure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable(nameof(Category));
            builder.HasKey(cat => cat.Id);
            builder.Property(cat => cat.IsDeleted).IsRequired();

            builder.Property(cat => cat.Id).IsRequired();
            builder.Property(cat => cat.CategoryName).IsRequired().HasMaxLength(100);
            builder.HasIndex(cat => cat.CategoryName).IsUnique(false);

            builder.Property(cat => cat.Description).HasMaxLength(int.MaxValue);
        }

        private void BrandConfigure(EntityTypeBuilder<Brand> builder)
        {
            builder.ToTable(nameof(Brand));
            builder.HasKey(b => b.Id);

            builder.Property(b => b.Id).IsRequired();
            builder.Property(b => b.IsDeleted).IsRequired();

            builder.Property(b => b.BrandName).IsRequired().HasMaxLength(100);
            builder.HasIndex(b => b.BrandName).IsUnique(false);

            builder.Property(b => b.Description).IsRequired(false).HasMaxLength(int.MaxValue);
        }

        private void ProductConfigure(EntityTypeBuilder<Entities.Product> builder)
        {
            builder.ToTable(nameof(Entities.Product));

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.IsDeleted).IsRequired();
            builder.Property(p => p.ActualQuantity).IsRequired();
            builder.Property(p => p.Code).IsRequired().HasMaxLength(100);
            builder.HasIndex(p => p.Code).IsUnique();

            builder.Property(p => p.Name).IsRequired().HasMaxLength(100);

            builder.Property(p => p.Description).IsRequired(false).HasMaxLength(int.MaxValue);
            builder.Property(p => p.NotInCartQuantity).IsRequired();
            builder.Property(p => p.PictureUri).IsRequired(false);
            builder.Property(p => p.Price).IsRequired();

            builder.HasOne(p => p.Category).WithMany().HasForeignKey(p => p.CategoryId);
            builder.HasIndex(p => p.CategoryId).IsUnique(false);

            builder.HasOne(p => p.SubCategory).WithMany().HasForeignKey(p => p.SubCategoryId);
            builder.HasIndex(p => p.SubCategoryId).IsUnique(false);

            builder.HasOne(p => p.Brand).WithMany().HasForeignKey(p => p.BrandId);
            builder.HasIndex(p => p.BrandId).IsUnique(false);
        }
    }
}
