﻿using EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using Product.Domain.Entities;
using Service.Common.Events.ProductEvents;
using Service.Common.Logging;
using ST.Library.Base;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Product.Service.EventHandling
{
    public class ProductAddToCartIntegrationEventHandler : IIntegrationEventHandler<ProductAddToCartIntegrationEvent>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<ProductAddToCartIntegrationEventHandler> _logger;

        public ProductAddToCartIntegrationEventHandler(IUnitOfWork unitOfWork, ILogger<ProductAddToCartIntegrationEventHandler> log)
        {
            _unitOfWork = unitOfWork;
            _logger = log;
        }
        public async Task Handle(ProductAddToCartIntegrationEvent @event)
        {
            try
            {
                var productRepository = _unitOfWork.GetRepository<Domain.Entities.Product>();
                var product = productRepository.Find(@event.ProductId);
                if (product != null)
                {
                    product.NotInCartQuantity -= @event.ProductAdded;
                    productRepository.Update(product);

                    string message = $"ProductId[{@event.ProductId}] added {@event.ProductAdded} to cart";
                    _unitOfWork.GetRepository<ProductHistory>().Insert(new ProductHistory
                    {
                        ProductId = @event.ProductId,
                        Action = Domain.Enum.ProductChangedAction.AddedProductToCart,
                        Description = message,
                        CreatedOn = @event.CreationDate,
                        CorrelationId = @event.CorrelationId,
                        ChangedValue = @event.ProductAdded,
                        ActionBy = @event.ActionBy
                    });
                    _logger.LogInformation(message);
                }
                else
                {
                    _logger.LogError(LoggingEvents.GET_ITEM_NOTFOUND, $"Can't find the productId[{@event.ProductId}]. The CorrelationId[{@event.CorrelationId}] at [{@event.CreationDate.ToString()}]");
                }
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.HANDLE_EVENT_ERROR, ex, ex.Message);
            }

        }
    }
}
