﻿using EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using Product.Domain.Entities;
using Service.Common.Events.ProductEvents;
using Service.Common.Logging;
using ST.Library.Base.UnitOfWork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Service.EventHandling
{
    public class ProductOrderedIntegrationEventHandler : IIntegrationEventHandler<ProductOrderedIntegrationEvent>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<ProductOrderedIntegrationEventHandler> _logger;

        public ProductOrderedIntegrationEventHandler(IUnitOfWork unitOfWork, ILogger<ProductOrderedIntegrationEventHandler> log)
        {
            _unitOfWork = unitOfWork;
            _logger = log;
        }
        public async Task Handle(ProductOrderedIntegrationEvent @event)
        {
            try
            {
                var productRepository = _unitOfWork.GetRepository<Domain.Entities.Product>();

                var updateProducts = productRepository.GetAll(p => @event.ProductsOrdered.Keys.Contains(p.Id)).AsEnumerable().Select(p =>
                {
                    p.ActualQuantity -= @event.ProductsOrdered[p.Id];
                    return p;
                });
                productRepository.Update(updateProducts);
                
                _unitOfWork.GetRepository<ProductHistory>().Insert(updateProducts.Select(p => new ProductHistory
                {
                    ActionBy = @event.ActionBy,
                    CorrelationId = @event.CorrelationId,
                    ProductId = p.Id,
                    Action = Domain.Enum.ProductChangedAction.OrderedProduct,
                    ChangedValue = @event.ProductsOrdered[p.Id],
                    Description = $"ProductId[{p.Id}] Ordered {@event.ProductsOrdered[p.Id]}"
                }));

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.HANDLE_EVENT_ERROR, ex, ex.Message);
            }
        }
    }
}
