﻿using EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using Product.Domain.Entities;
using Service.Common.Events.ProductEvents;
using Service.Common.Logging;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Product.Service.EventHandling
{
    public class ProductRemoveFromCartIntegrationEventHandler : IIntegrationEventHandler<ProductRemoveFromCartIntegrationEvent>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<ProductRemoveFromCartIntegrationEventHandler> _logger;
        public ProductRemoveFromCartIntegrationEventHandler(IUnitOfWork unitOfWork, ILogger<ProductRemoveFromCartIntegrationEventHandler> log)
        {
            _unitOfWork = unitOfWork;
            _logger = log;
        }
        public async Task Handle(ProductRemoveFromCartIntegrationEvent @event)
        {
            try
            {
                var productRepository = _unitOfWork.GetRepository<Domain.Entities.Product>();
                var product = productRepository.Find(@event.ProductId);
                if (product != null)
                {
                    product.NotInCartQuantity += @event.ProductRemoved;
                    if (product.NotInCartQuantity > product.ActualQuantity)
                    {
                        product.NotInCartQuantity = product.ActualQuantity;
                        _logger.LogWarning($"Product[{@event.ProductId}] that has more quantity in cart than actual!. Force the maximum value to Actual.");
                    }
                    productRepository.Update(product);
                    
                    _unitOfWork.GetRepository<ProductHistory>().Insert(new ProductHistory
                    {
                        ProductId = @event.ProductId,
                        Action = Domain.Enum.ProductChangedAction.RemoveProductFromCart,
                        Description = @event.Note,
                        CreatedOn = @event.CreationDate,
                        CorrelationId = @event.CorrelationId,
                        ChangedValue = @event.ProductRemoved,
                        ActionBy = @event.ActionBy
                    });
                    _logger.LogInformation($"ProductId[{@event.ProductId}] removed {@event.ProductRemoved} from cart. Note: {@event.Note}");
                }
                else
                {
                    _logger.LogError(LoggingEvents.GET_ITEM_NOTFOUND, $"Can't find the productId[{@event.ProductId}]. The CorrelationId[{@event.CorrelationId}] at [{@event.CreationDate.ToString()}]");
                }
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.HANDLE_EVENT_ERROR, ex, ex.Message);
            }
        }
    }
}
