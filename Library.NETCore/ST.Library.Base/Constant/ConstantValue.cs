﻿namespace ST.Library.Base.Constant
{
    public static class ConstantValue
    {
        //TODO should make more case for this number to fastest number - maybe can use a simple machine learning
        public const int COMMIT_COUNT_TO_RECREATED_CONTEXT = 1000; // If commit entities more than 1000 record then created context
    }
}