﻿using System;
using System.Linq.Expressions;

namespace ST.Library.Base.Entity
{
    public interface ISecuredCondition<TEntity> where TEntity : BaseEntity
    {
        Expression<Func<TEntity, bool>> SecuredCondition { get; }
    }
}