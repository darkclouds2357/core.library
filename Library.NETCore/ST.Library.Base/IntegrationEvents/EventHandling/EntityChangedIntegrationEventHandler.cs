﻿using EventBus.Abstractions;
using ST.Library.Base.Entity;
using ST.Library.Base.Enum;
using ST.Library.Base.IntegrationEvents.Events;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ST.Library.Base.IntegrationEvents.EventHandling
{
    public class EntityChangedIntegrationEventHandler : IIntegrationEventHandler<EntityChangedIntegrationEvent>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Dictionary<string, (Type entityType, Dictionary<string, string> columns)> _entitiesTypeMap;

        public EntityChangedIntegrationEventHandler(IUnitOfWork unitOfWork, Dictionary<string, (Type, Dictionary<string, string>)> entitiesMap)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.SetEventChangedFlag(true);
            _entitiesTypeMap = entitiesMap;
        }
        public async Task Handle(EntityChangedIntegrationEvent message)
        {
            /// process entity message
            /// 
            if (_entitiesTypeMap != null && _entitiesTypeMap.ContainsKey(message.EnityName))
            {
                var entityStruct = _entitiesTypeMap[message.EnityName];
                if (entityStruct.entityType.GetTypeInfo().BaseType.Equals(typeof(BaseEntity)))
                {
                    var repository = _unitOfWork.GetType().GetMethod(nameof(_unitOfWork.GetRepository))?.MakeGenericMethod(entityStruct.entityType)?.Invoke(_unitOfWork, null);
                    IEnumerable<object> objs = message.Entities.Select(e => MapEntity(e, entityStruct.entityType, entityStruct.columns));

                    switch (message.Action)
                    {
                        case EntityAction.Insert:
                            repository.GetType().GetMethod("Insert").Invoke(repository, new object[] { objs });
                            break;
                        case EntityAction.Delete:
                            repository.GetType().GetMethod("HardDelete").Invoke(repository, new object[] { objs });
                            break;
                        case EntityAction.Update:
                            /// should check if any use column change data
                            /// if don't have any use column has changed on data then ingore them
                            repository.GetType().GetMethod("Update").Invoke(repository, new object[] { objs });
                            break;
                    }
                    await _unitOfWork.SaveChangesAsync();
                }
            }
            _unitOfWork.SetEventChangedFlag(false);
        }

        private object MapEntity(object srcObj, Type desType, Dictionary<string, string> mapColumns)
        {
            var desObj = Activator.CreateInstance(desType);
            var props = srcObj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if (mapColumns.ContainsKey(prop.Name))
                {
                    var desProp = desType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty).FirstOrDefault(p => p.Name == mapColumns[prop.Name]);
                    if (desProp != null)
                    {
                        desProp.SetValue(desObj, prop.GetValue(srcObj));
                    }
                }
            }
            return desObj;
        }
        
    }
}
