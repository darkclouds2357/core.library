﻿using EventBus.Event;
using ST.Library.Base.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.Library.Base.IntegrationEvents.Events
{
    public class EntityChangedIntegrationEvent: IntegrationEvent
    {
        public EntityAction Action { get; set; }
        public string EnityName { get; set; }

        public IEnumerable<object> Entities { get; set; }
    }
}
