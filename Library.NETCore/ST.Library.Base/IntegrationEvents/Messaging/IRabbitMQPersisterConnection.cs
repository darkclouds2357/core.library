﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.Library.Base.Messaging
{
    public interface IRabbitMQPersisterConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}
