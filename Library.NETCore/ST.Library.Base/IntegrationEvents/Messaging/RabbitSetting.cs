﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ST.Library.Base.IntegrationEvents.Messaging
{
    public class RabbitSetting
    {
        public string Uri { get; set; }
        public string EventBusConnection { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }        
    }
}
