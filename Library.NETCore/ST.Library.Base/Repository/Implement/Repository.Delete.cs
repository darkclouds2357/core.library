﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ST.Library.Base
{
    public partial class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        public virtual void Delete(TEntity entity)
        {
            if (ValidateRules())
                Update(SetSoftDeleteEntity(entity));
        }

        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            if (ValidateRules())
                Update(entities.Select(entity => SetSoftDeleteEntity(entity)));
        }

        public virtual void Delete(params TEntity[] entities)
        {
            if (ValidateRules())
                Update(entities.Select(entity => SetSoftDeleteEntity(entity)));
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> criteria)
        {
            if (ValidateRules())
            {
                var entities = _dbSet.Where(criteria);
                Delete(entities);
            }
        }

        public virtual void Delete(object id)
        {
            if (ValidateRules())
            {
                var entity = _dbSet.Find(id);
                if (entity != null)
                {
                    Delete(entity);
                }
            }
        }

        public virtual void HardDelete(object id)
        {
            if (ValidateRules())
            {
                var typeInfo = typeof(TEntity).GetTypeInfo();
                var key = _context.Model.FindEntityType(typeInfo.Name).FindPrimaryKey().Properties.FirstOrDefault();
                var property = typeInfo.GetProperty(key?.Name);
                if (property != null)
                {
                    var entity = Activator.CreateInstance<TEntity>();
                    property.SetValue(entity, id);
                    _context.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    var entity = _dbSet.Find(id);
                    if (entity != null)
                    {
                        HardDelete(entity);
                    }
                }
            }
        }

        public virtual void HardDelete(TEntity entity)
        {
            if (ValidateRules())
                _dbSet.Remove(entity);
        }

        public virtual void HardDelete(IEnumerable<TEntity> entites)
        {
            if (ValidateRules())
                _dbSet.RemoveRange(entites);
        }

        public virtual void HardDelete(params TEntity[] entites)
        {
            if (ValidateRules())
                _dbSet.RemoveRange(entites);
        }

        public virtual void HardDelete(Expression<Func<TEntity, bool>> criteria)
        {
            if (ValidateRules())
                HardDelete(_dbSet.Where(criteria));
        }
    }
}