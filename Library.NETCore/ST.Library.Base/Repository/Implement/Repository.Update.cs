﻿using ST.Library.Base.Entity;
using System.Collections.Generic;
using System.Linq;

namespace ST.Library.Base
{
    public partial class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        public virtual void Update(TEntity entity)
        {
            if (ValidateRules())
            {
                if (entity.IsDeleted && !_canSoftDelete)
                {
                    HardDelete(entity);
                    return;
                }
                _dbSet.Update(SetUpdateEntity(entity));
            }
        }

        public virtual void Update(IEnumerable<TEntity> entities)
        {
            if (ValidateRules())
            {
                var deleteEntities = entities.Where(e => e.IsDeleted);
                if (!_canSoftDelete)
                {
                    HardDelete(deleteEntities);
                    entities = entities.Except(deleteEntities);
                    if (entities.Count() == 0)
                    {
                        return;
                    }
                }
                _dbSet.UpdateRange(entities.Select(entity => SetUpdateEntity(entity)));
            }
        }

        public virtual void Update(params TEntity[] entities)
        {
            if (ValidateRules())
                _dbSet.UpdateRange(entities.Select(entity => SetUpdateEntity(entity)));
        }
    }
}