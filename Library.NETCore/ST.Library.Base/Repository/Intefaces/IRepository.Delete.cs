﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ST.Library.Base
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        void Delete(object id);

        void Delete(TEntity entity);

        void Delete(params TEntity[] entities);

        void Delete(IEnumerable<TEntity> entities);

        void Delete(Expression<Func<TEntity, bool>> criteria);

        void HardDelete(object id);

        void HardDelete(TEntity entity);

        void HardDelete(params TEntity[] entites);

        void HardDelete(IEnumerable<TEntity> items);

        void HardDelete(Expression<Func<TEntity, bool>> criteria);
    }
}