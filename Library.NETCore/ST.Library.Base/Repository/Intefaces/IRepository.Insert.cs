﻿using ST.Library.Base.Entity;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ST.Library.Base
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        void BulkInsert(IEnumerable<TEntity> items);

        Task BulkInsertAsync(IEnumerable<TEntity> entities);

        void Insert(TEntity entity);

        void Insert(IEnumerable<TEntity> entities);

        void Insert(params TEntity[] entities);

        Task InsertAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken));

        Task InsertAsync(params TEntity[] entities);

        Task InsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken));
    }
}