﻿using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using ST.Library.Base.Enum;
using ST.Library.Base.IntegrationEvents.EventHandling;
using ST.Library.Base.IntegrationEvents.Events;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ST.Library.Base.UnitOfWork
{
    public partial class UnitOfWork<TCtx> : IUnitOfWork, IDisposable where TCtx : DbContext
    {
        private bool _isEventChange = false;
        public void SetEventChangedFlag(bool flag = false, [CallerMemberName]string callFrom = "")
        {
            /// must check that the set event changed flag only call from EntityChangedIntegrationEventHandler
            if (callFrom == nameof(EntityChangedIntegrationEventHandler))
            {
                _isEventChange = flag;
            }           
        }

        private void PublishThroughEventBus(EntityAction action, string entityName, object[] dtoEntites)
        {
            /// won't publish event when the event changed flag is on 
            /// (EntityChangedIntegrationEventHandler is handle for insert data)
            /// or this is the update event to entity that shared from other service
            if (!_isEventChange)
            {
                EntityChangedIntegrationEvent message = new EntityChangedIntegrationEvent()
                {
                    Action = action,
                    EnityName = entityName,
                    Entities = dtoEntites
                };
                _eventBus.Publish(message, ExchangeType.Fanout);
            }

        }
    }
}
