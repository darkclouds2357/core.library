﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Threading.Tasks;

namespace ST.Library.Common.Caching
{
    public static class CacheExtensions
    {
        private static int? _cacheTimeInMinute;

        private static int CacheTime
        {
            get
            {
                if (!_cacheTimeInMinute.HasValue && CacheOption.Instance.CacheTime != CacheOption.INVALID_CACHE_TIME)
                {
                    int time = CacheOption.Instance.CacheTime;
                    _cacheTimeInMinute = time;
                }
                return _cacheTimeInMinute ?? CacheOption.DEFAULT_CACHE_TIME;
            }
        }

        private static CacheDataType _cacheDataType => CacheOption.Instance.Type;

        public static T GetCache<T>(this IDistributedCache cacheManager, string key)
        {
            return cacheManager.GetCache<T>(key, CacheOption.DEFAULT_CACHE_RESET_TIME, _cacheDataType);
        }

        public static T GetCache<T>(this IDistributedCache cacheManager, string key, Func<T> acquire)
        {
            return GetCache(cacheManager, key, CacheTime, acquire);
        }

        public static T GetCache<T>(this IDistributedCache cacheManager, string key, int cacheTime, Func<T> acquire)
        {
            T result = cacheManager.GetCache<T>(key, CacheOption.DEFAULT_CACHE_RESET_TIME, _cacheDataType);
            if (result != null)
            {
                return result;
            }
            else
            {
                result = acquire();
                cacheManager.SetCache(key, result, cacheTime, _cacheDataType);
                return result;
            }
        }

        public static async Task<T> GetCacheAsync<T>(this IDistributedCache cacheManager, string key, Func<Task<T>> acquire)
        {
            return await GetCacheAsync(cacheManager, key, CacheTime, acquire);
        }

        public static async Task<T> GetCacheAsync<T>(this IDistributedCache cacheManager, string key, int cacheTime, Func<Task<T>> acquire)
        {
            T result = await cacheManager.GetCacheAsync<T>(key, CacheOption.DEFAULT_CACHE_RESET_TIME, _cacheDataType);
            if (result != null)
            {
                return result;
            }
            else
            {
                result = await acquire();
                cacheManager.SetCacheAsync(key, result, cacheTime, _cacheDataType);
                return result;
            }
        }
    }
}