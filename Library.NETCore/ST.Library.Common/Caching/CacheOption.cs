﻿namespace ST.Library.Common.Caching
{
    public partial class CacheOption
    {
        private static readonly object _lock = new object();
        private static CacheOption _instance;

        public static CacheOption Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new CacheOption()
                        {
                            CacheTime = INVALID_CACHE_TIME,
                            Type = CacheDataType.Json
                        };
                    }

                    return _instance;
                }
            }
        }

        /// <summary>
        /// CacheTime (in minute)
        /// </summary>
        public int CacheTime { get; set; }

        public CacheDataType Type { get; private set; }

        // Set Default Rule for Cache key
        public static string CacheKeyRule(string key, CacheDataType dataType)
        {
            return $"{key}.{dataType.ToString()}";
        }
    }
}