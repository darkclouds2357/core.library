﻿namespace ST.Library.Common.Caching
{
    public class RedisOption
    {
        // TODO should check Redis Exist in private via Configuration and Instance Name ????
        public bool IsExist { get; set; }

        public string Configuration { get; set; }
        public string InstanceName { get; set; }
    }
}