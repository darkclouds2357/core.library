﻿/// <summary>
/// Can't not find System.ComponentModel in nuget for .NET core
/// And I often use ListSortDirection enum, so make new one :)
/// </summary>
namespace System.ComponentModel
{
    //
    // Summary:
    //     Specifies the direction of a sort operation.
    public enum ListSortDirection
    {
        //
        // Summary:
        //     Sorts in ascending order.
        Ascending = 0,

        //
        // Summary:
        //     Sorts in descending order.
        Descending = 1
    }
}