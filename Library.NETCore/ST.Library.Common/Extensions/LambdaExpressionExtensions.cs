﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LambdaExpressionExtensions.cs" company="Hunter Macdonald">
//   Hunter Macdonald copyright 2016
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// Note: this one I get from my ex-company: "Hunter Macdonald Vietnam" as same as many functions in Common library,
// they are very useful
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ST.Library.Common.Extensions
{
    /// <summary>
    /// The lambda expression extensions.
    /// </summary>
    public static class LambdaExpressionExtensions
    {
        #region Public Methods and Operators

        public static IQueryable<TDto> Select<T, TDto>(this IQueryable<T> source)
        {
            return source.Select(CreateDtoStatement<T, TDto>());
        }

        private static Expression<Func<T, TDto>> CreateDtoStatement<T, TDto>()
        {
            var source = typeof(T);
            var dest = typeof(TDto);
            List<PropertyInfo> props = new List<PropertyInfo>();
            foreach (var dp in dest.GetProperties())
            {
                var sp = source.GetProperty(dp.Name);
                if (sp != null && sp.GetCustomAttribute<NotMappedAttribute>() == null
                        && (sp.PropertyType == dp.PropertyType || sp.PropertyType == Nullable.GetUnderlyingType(dp.PropertyType)))
                    props.Add(dp);
            }
            string fields = string.Join(",", props.Select(x => x.Name));
            return CreateNewStatement<T, TDto>(fields, source, dest);
        }

        internal static Expression<Func<T, TDto>> CreateNewStatement<T, TDto>(string fields, Type source = null, Type dest = null)
        {
            source = source ?? typeof(T);
            dest = dest ?? typeof(TDto);

            // input parameter "o"
            var xParameter = Expression.Parameter(source, "o");

            // new statement "new Data()"
            var xNew = Expression.New(dest);

            // create initializers
            var bindings = fields.Split(',').Select(o => o.Trim())
                .Select(o =>
                {
                    // property "Field1"
                    var sourceProp = source.GetProperty(o);
                    var destProp = dest.GetProperty(o);
                    // original value "o.Field1"
                    var xOriginal = Expression.Property(xParameter, sourceProp);
                    // set value "Field1 = o.Field1"
                    return Expression.Bind(destProp, xOriginal);
                }
            );

            // initialization "new Data { Field1 = o.Field1, Field2 = o.Field2 }"
            var xInit = Expression.MemberInit(xNew, bindings);

            // expression "o => new Data { Field1 = o.Field1, Field2 = o.Field2 }"
            var lambda = Expression.Lambda<Func<T, TDto>>(xInit, xParameter);

            // compile to Func<Data, Data>
            return lambda;
        }

        #endregion Public Methods and Operators
    }
}