﻿using System.Text.RegularExpressions;

namespace ST.Library.Common.Helper
{
    public class HtmlHelper
    {
        private static readonly Regex HtmlDetectionRegex = new Regex("<(.*\\s*)>", RegexOptions.Compiled);
        private static readonly Regex StripWhiteSpaceRegex = new Regex("\\s+", RegexOptions.Compiled);
        private static readonly Regex StripNonWordRegex = new Regex("\\W*", RegexOptions.Compiled);
        private static readonly Regex StripTagsRegex = new Regex("<[^<>]*>", RegexOptions.Compiled);
        private static readonly Regex RemoveScriptRegEx = new Regex(@"<script[^>]*>[\s\S]*?</script>", RegexOptions.Compiled);
        private static readonly Regex RemoveInlineStylesRegEx = new Regex("<style>.*?</style>", RegexOptions.Compiled | RegexOptions.Multiline);

        //Match all variants of <br> tag (<br>, <BR>, <br>, including embedded space
        private static readonly Regex ReplaceHtmlNewLinesRegex = new Regex("\\s*<\\s*[bB][rR]\\s*/\\s*>\\s*", RegexOptions.Compiled);

        //Create Regular Expression objects
        private const string PunctuationMatch = "[~!#\\$%\\^&*\\(\\)-+=\\{\\[\\}\\]\\|;:\\x22'<,>\\.\\?\\\\\\t\\r\\v\\f\\n]";

        private static readonly Regex AfterRegEx = new Regex(PunctuationMatch + "\\s", RegexOptions.Compiled);
        private static readonly Regex BeforeRegEx = new Regex("\\s" + PunctuationMatch, RegexOptions.Compiled);
        private static readonly Regex EntityRegEx = new Regex("&[^;]+;", RegexOptions.Compiled);

        public static string CleanScriptTags(string html, bool removePunctuation)
        {
            html = RemoveScriptRegEx.Replace(html, " ");
            if (removePunctuation)
            {
                html = StripPunctuation(html, true);
            }
            return html;
        }

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// StripPunctuation removes the Punctuation from the content
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="HTML">The HTML content to clean up</param>
        /// <param name="RetainSpace">Indicates whether to replace the Punctuation by a space (true) or nothing (false)</param>
        /// <returns>The cleaned up string</returns>
        /// -----------------------------------------------------------------------------
        public static string StripPunctuation(string HTML, bool RetainSpace)
        {
            if (string.IsNullOrWhiteSpace(HTML))
            {
                return string.Empty;
            }

            //Define return string
            string retHTML = HTML + " "; //Make sure any punctuation at the end of the String is removed

            //Set up Replacement String
            var repString = RetainSpace ? " " : "";
            while (BeforeRegEx.IsMatch(retHTML))
            {
                retHTML = BeforeRegEx.Replace(retHTML, repString);
            }
            while (AfterRegEx.IsMatch(retHTML))
            {
                retHTML = AfterRegEx.Replace(retHTML, repString);
            }
            // Return modified string after trimming leading and ending quotation marks
            return retHTML.Trim('"');
        }
    }
}