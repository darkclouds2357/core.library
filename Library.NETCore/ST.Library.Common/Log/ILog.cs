﻿using System;

namespace ST.Library.Common.Log
{
    public interface ILogBase
    {
        void Trace(string message);

        void Trace(string message, Exception exc);

        void Debug(string message);

        void Debug(string message, Exception exc);

        void Info(string message);

        void Info(string message, Exception exc);

        void Warn(string message);

        void Warn(string message, Exception exc);

        void Error(string message);

        void Error(string message, Exception exc);

        void Fatal(string message);

        void Fatal(string message, Exception exc);
    }

    public interface ILog<T> : ILogBase
    {
    }
}