﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace ST.Library.Common.Sort
{
    public class ExpressionSortCriteria<T, TKey> : ISortCriteria<T>
    {
        public Expression<Func<T, TKey>> SortExpression { get; set; }

        public ListSortDirection Direction { get; set; }

        public ExpressionSortCriteria()
        {
            Direction = ListSortDirection.Ascending;
        }

        public ExpressionSortCriteria(Expression<Func<T, TKey>> expression, ListSortDirection direction)
        {
            SortExpression = expression;
            Direction = direction;
        }

        public IOrderedQueryable<T> ApplyOrdering(IQueryable<T> query, Boolean useThenBy)
        {
            IOrderedQueryable<T> result = null;
            if (SortExpression != null)
            {
                if (Direction == ListSortDirection.Ascending)
                {
                    result = !useThenBy ? query.OrderBy(SortExpression) : ((IOrderedQueryable<T>)query).ThenBy(SortExpression);
                }
                else
                {
                    result = !useThenBy ? query.OrderByDescending(SortExpression) : ((IOrderedQueryable<T>)query).ThenByDescending(SortExpression);
                }
            }
            else
            {
                return query.OrderBy(x => x);
            }
            return result;
        }
    }
}