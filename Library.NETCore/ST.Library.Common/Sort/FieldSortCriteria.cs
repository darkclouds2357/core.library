﻿using System;
using System.ComponentModel;
using System.Linq;

namespace ST.Library.Common.Sort
{
    public class FieldSortCriteria<T> : ISortCriteria<T> where T : class
    {
        public String Name { get; set; }

        public ListSortDirection Direction { get; set; }

        public FieldSortCriteria()
        {
            Direction = ListSortDirection.Ascending;
        }

        public FieldSortCriteria(String name, ListSortDirection direction)
           : base()
        {
            Name = name;
            Direction = direction;
        }

        public IOrderedQueryable<T> ApplyOrdering(IQueryable<T> qry, Boolean useThenBy)
        {
            IOrderedQueryable<T> result = null;
            var descending = this.Direction == ListSortDirection.Descending;
            result = !useThenBy ? qry.OrderBy(Name, descending) : qry.ThenBy(Name, descending);
            return result;
        }
    }
}