﻿using System;
using System.ComponentModel;
using System.Linq;

namespace ST.Library.Common.Sort
{
    public interface ISortCriteria<T>
    {
        ListSortDirection Direction { get; set; }

        IOrderedQueryable<T> ApplyOrdering(IQueryable<T> query, Boolean useThenBy);
    }
}