﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Reflection;
using ST.Library.Base.Entity;
using ST.Library.Extension.Entity;
using Microsoft.Extensions.Caching.Distributed;
using ST.Library.Common.Caching;
namespace ST.Library.Extension
{
    public static class DependencyInjection
    {
        private static Dictionary<string, Type> Entities = null;
        private static object _lock;

        public static IServiceCollection AddPermissionRule(this IServiceCollection services, string connectionString, string serviceName)
        {
            var context = new PermissionRuleContext(new DbContextOptionsBuilder<PermissionRuleContext>()
               .UseSqlServer(connectionString, b => b.MigrationsAssembly("ST.Library.Extension")).Options);
            context.Database.Migrate();

            if (!context.PermissionEntities.Any())
            {
                Dictionary<string, Type> entities = GetEntities();
                var permissions = entities.Select(p => new PermissionEntity
                {
                    EntityName = p.Key,
                    Id = Guid.NewGuid(),
                    IsDelete = false,
                    IsInsert = false,
                    IsUpdate = false
                });
                context.PermissionEntities.AddRange(permissions);
                context.SaveChanges();
            }
            context.Dispose();
            /// Inject all Permission List
            /// Get permission rule item from cache or read from the json setting (or event get from database)
            services.AddTransient<IEnumerable<PermissionRuleItem>>(sp =>
            {
                var cache = sp.GetRequiredService<IDistributedCache>();
                return cache.GetCache($"{serviceName}.{nameof(PermissionRuleItem)}", () =>
                {
                    var ctx = new PermissionRuleContext(new DbContextOptionsBuilder<PermissionRuleContext>().UseSqlServer(connectionString).Options);
                    var entitiesType = GetEntities();
                    return ctx.PermissionEntities.ToList().Select(p =>
                    {
                        if (entitiesType.ContainsKey(p.EntityName))
                        {
                            return new PermissionRuleItem(entityType: entitiesType[p.EntityName], canDelete: p.IsDelete, canInsert: p.IsInsert, canUpdate: p.IsUpdate, canView: true);
                        }
                        return null;
                    }).Where(e => e != null).ToList();
                });
            });

            return services;
        }


        public static Dictionary<string, Type> GetEntities()
        {
            lock (_lock)
            {
                if (Entities == null)
                {
                    Entities = new Dictionary<string, Type>();
                    var entitiesType = Assembly
                            .GetEntryAssembly()
                            .GetReferencedAssemblies()
                            .Select(Assembly.Load)
                            .SelectMany(x => x.DefinedTypes)
                            .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(BaseEntity)))
                            .Select(x => x.AsType());
                    foreach (var type in entitiesType)
                    {
                        //isShareEntity = type.GetInterfaces()?.Any(i => i.Equals(typeof(IShareEntity))) ?? isShareEntity;
                        Entities.Add(type.Name, type);
                    }
                }
                return Entities;
            }

        }
    }
}
