﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ST.Library.Extension.Entity
{
    public class PermissionEntity
    {   
        [Key]
        public Guid Id { get; set; }

        public string EntityName { get; set; }

        public bool IsInsert { get; set; }

        public bool IsUpdate { get; set; }

        public bool IsDelete { get; set; }
    }
}
