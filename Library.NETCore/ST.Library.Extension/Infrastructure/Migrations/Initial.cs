﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;
using ST.Library.Extension.Entity;

namespace ST.Library.Extension.Infrastructure.Migrations
{
    public class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(PermissionEntity),
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, name: nameof(PermissionEntity.Id)),
                    EntityName = table.Column<string>(maxLength: 100, nullable: false, name: nameof(PermissionEntity.EntityName)),
                    IsInsert = table.Column<bool>(nullable: false, defaultValue: false, name: nameof(PermissionEntity.IsInsert)),
                    IsUpdate = table.Column<bool>(nullable: false, defaultValue: false, name: nameof(PermissionEntity.IsUpdate)),
                    IsDelete = table.Column<bool>(nullable: false, defaultValue: false, name: nameof(PermissionEntity.IsDelete))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionEntity", x => x.Id);
                });
            migrationBuilder.CreateIndex(
                name: "IX_PermissionEntity_EntityName",
                table: nameof(PermissionEntity),
                column: nameof(PermissionEntity.EntityName));
        }
    }
}
