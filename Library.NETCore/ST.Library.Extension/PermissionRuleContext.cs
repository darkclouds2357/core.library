﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ST.Library.Extension.Entity;


namespace ST.Library.Extension
{
    public class PermissionRuleContext : DbContext
    {
        public PermissionRuleContext(DbContextOptions<PermissionRuleContext> options) : base(options)
        {

        }
        public DbSet<PermissionEntity> PermissionEntities { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
            modelBuilder.Entity<PermissionEntity>(Configure);
        }

        void Configure(EntityTypeBuilder<PermissionEntity> builder)
        {
            builder.ToTable(nameof(PermissionEntity));

            builder.HasKey(pe => pe.Id);

            builder.Property(pe => pe.Id)
               .IsRequired();

            builder.Property(pe => pe.EntityName)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(pe => pe.IsInsert).IsRequired().HasDefaultValue(false);
            builder.Property(pe => pe.IsUpdate).IsRequired().HasDefaultValue(false);
            builder.Property(pe => pe.IsUpdate).IsRequired().HasDefaultValue(false);
        }
    }
}
