﻿using EventBus.Event;

namespace ST.Library.Messaging.EventService
{
    public interface IIntegrationEventService
    {
        void PublishThroughEventBus(IntegrationEvent @event);
    }
}