﻿using EventBus.Abstractions;
using EventBus.Event;
using ST.Library.Base.UnitOfWork;
using System;

namespace ST.Library.Messaging.EventService
{
    public class IntegrationEventService : IIntegrationEventService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEventBus _eventBus;

        public IntegrationEventService(IUnitOfWork unitOfWork, IEventBus eventBus)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void PublishThroughEventBus(IntegrationEvent @event)
        {
            _eventBus.Publish(@event);
        }
    }
}