﻿using RabbitMQ.Client;
using System;

namespace ST.Library.Messaging
{
    public interface IRabbitMQPersistentConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}