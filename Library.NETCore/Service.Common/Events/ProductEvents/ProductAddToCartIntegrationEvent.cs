﻿using EventBus.Event;
using System;

namespace Service.Common.Events.ProductEvents
{
    public class ProductAddToCartIntegrationEvent : IntegrationEvent
    {
        public Guid ProductId { get; private set; }

        public int ProductAdded { get; private set; }

        public ProductAddToCartIntegrationEvent(Guid productId, int productAdded, string actionBy)
        {
            ProductId = productId;
            ProductAdded = productAdded;
            ActionBy = actionBy;
        }
    }
}
