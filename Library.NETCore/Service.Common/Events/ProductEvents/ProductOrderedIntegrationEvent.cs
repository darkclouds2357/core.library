﻿using EventBus.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Common.Events.ProductEvents
{
    public class ProductOrderedIntegrationEvent : IntegrationEvent
    {

        public Dictionary<Guid, int> ProductsOrdered { get; private set; }

        public ProductOrderedIntegrationEvent(Dictionary<Guid, int> productsOrdered, string actionBy)
        {
            ProductsOrdered = productsOrdered;
            ActionBy = actionBy;
        }
    }
}
