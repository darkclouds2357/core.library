﻿using EventBus.Event;
using System;

namespace Service.Common.Events.ProductEvents
{
    public class ProductPriceChangedIntegrationEvent : IntegrationEvent
    {
        public Guid ProductId { get; private set; }

        public decimal NewValue { get; private set; }

        public decimal OldValue { get; private set; }

        public ProductPriceChangedIntegrationEvent(Guid productId, decimal newValue, decimal oldValue, string actionBy)
        {
            ProductId = productId;
            NewValue = newValue;
            OldValue = oldValue;
            ActionBy = actionBy;
        }
    }
}
