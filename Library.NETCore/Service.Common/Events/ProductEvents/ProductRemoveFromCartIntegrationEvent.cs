﻿using EventBus.Event;
using System;

namespace Service.Common.Events.ProductEvents
{
    public class ProductRemoveFromCartIntegrationEvent : IntegrationEvent
    {
        public Guid ProductId { get; private set; }

        public int ProductRemoved { get; private set; }

        public string Note { get; private set; }

        public ProductRemoveFromCartIntegrationEvent(Guid productId, int productRemoved, string note, string actionBy)
        {
            ProductId = productId;
            ProductRemoved = productRemoved;
            Note = note;
            ActionBy = actionBy;
        }
    }
}
