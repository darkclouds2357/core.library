﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Common;
using System;

namespace ST.Library.Base.Test.DTO
{
    public class CurrentUser : ICurrentUser
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsSystemUser { get; set; }

        public int FrontEndGMTMinutes { get; set; }

        public void CommitHistory(DbContext dbContext)
        {
        }

        public void CommitHistory()
        {
        }

        public void Init(Action<ICurrentUser> acquire)
        {
        }

        public bool IsEndpointAuthorized(string endpoint, string method)
        {
            return false;
        }

        public void KeepHistory(object entity, bool saveInBaseRepository = false)
        {
        }

        public void KeepHistory(object entity, string tableName, bool saveInBaseRepository = false)
        {
        }
    }
}