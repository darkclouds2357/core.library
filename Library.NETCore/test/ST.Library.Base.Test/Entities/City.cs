﻿using ST.Library.Base.Entity;
using ST.Library.Base.Test.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace ST.Library.Base.Test.Entities
{
    public class City : BaseEntity, ISecuredCondition<City>
    {
        public string Name { get; set; }

        [ForeignKey(nameof(Country))]
        public long CountryId { get; set; }

        public long Population { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<Customer> Customers { get; set; } = new HashSet<Customer>();

        public Expression<Func<City, bool>> SecuredCondition => city => city.Population >= DataCreateHelper.MinPopulation;
    }
}