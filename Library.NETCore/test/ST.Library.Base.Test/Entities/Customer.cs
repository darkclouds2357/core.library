﻿using ST.Library.Base.Entity;
using ST.Library.Base.Test.Data;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace ST.Library.Base.Test.Entities
{
    public class Customer : BaseEntity, ISecuredCondition<Customer>
    {
        //[Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public override long Id { get; set; }

        //[Key]
        public string Email { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        [NotMapped]//[ForeignKey(nameof(City))]
        public long CityId { get; set; }

        public string Job { get; set; }

        [NotMapped]
        public virtual City City { get; set; }

        public Expression<Func<Customer, bool>> SecuredCondition => cus => cus.Age >= DataCreateHelper.MinAge;
    }
}