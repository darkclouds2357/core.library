﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Base.Test.Entities;

namespace ST.Library.Base.Test
{
    public class InMemoryContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=SMITHTRAN;Initial Catalog=NetCoreTest;Integrated Security=False;User ID=sa;Password=123;MultipleActiveResultSets=true");
            //optionsBuilder.UseInMemoryDatabase("test");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasOne(e => e.Country).WithMany();
            modelBuilder.Entity<City>().HasMany(e => e.Customers).WithOne();

            modelBuilder.Entity<User>().HasKey(e => new { e.Id });
            modelBuilder.Entity<City>().HasKey(e => new { e.Id });
            modelBuilder.Entity<Country>().HasKey(e => new { e.Id });
            modelBuilder.Entity<Customer>().HasKey(e => new { e.Id, e.Email });
        }
    }
}