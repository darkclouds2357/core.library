﻿using ST.Library.Base.Test.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ST.Library.Base.Test.Data
{
    public static class CityData
    {
        public static City GetRandomCity()
        {
            return ListCities[DataCreateHelper.Random.Next(CitiesCnt)];
        }

        public static IEnumerable<City> GenerateLargeListCities(int number)
        {
            for (int i = 0; i < number; i++)
            {
                int randomName = DataCreateHelper.Random.Next(3, 18);
                yield return new City
                {
                    Name = DataCreateHelper.RandomString(randomName)
                };
            }
        }

        public static IEnumerable<City> GetRandomCities(int cusNumber)
        {
            return Enumerable.Repeat(ListCities, cusNumber).Select(e => e[DataCreateHelper.Random.Next(e.Count)]);
        }

        public static readonly int CitiesCnt = DataCreateHelper.Random.Next(5000, 10000);
        public static Guid[] CityIds = ListCities.Select(e => e.Id).ToArray();
        private static object _lock = new object();
        private static List<City> _instance = null;

        public static List<City> ListCities
        {
            get
            {
                if (_lock == null) _lock = new object();
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new List<City>();
                        for (int i = 0; i < CitiesCnt; i++)
                        {
                            int cityNameLeng = DataCreateHelper.Random.Next(3, 18);
                            Guid countryId = CountryData.CountryIds[DataCreateHelper.Random.Next(CountryData.CountriesCnt)];
                            _instance.Add(new City
                            {
                                Name = DataCreateHelper.RandomString(cityNameLeng),
                                Population = DataCreateHelper.Random.Next(0, int.MaxValue)
                            });
                        }
                    }
                    return _instance;
                }
            }
        }
    }
}