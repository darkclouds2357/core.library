﻿using ST.Library.Base.Test.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ST.Library.Base.Test.Data
{
    public class CountryData
    {
        public static Country GetRandomCountry()
        {
            return ListCountries[DataCreateHelper.Random.Next(CountriesCnt)];
        }

        public static IEnumerable<Country> GenerateLargeListCountry(int number)
        {
            for (int i = 0; i < number; i++)
            {
                int randomName = DataCreateHelper.Random.Next(3, 18);
                yield return new Country
                {
                    Name = DataCreateHelper.RandomString(randomName),
                };
            }
        }

        public static IEnumerable<Country> GetRandomCountries(int cusNumber)
        {
            return Enumerable.Repeat(ListCountries, cusNumber).Select(e => e[DataCreateHelper.Random.Next(e.Count)]);
        }

        public static readonly int CountriesCnt = DataCreateHelper.Random.Next(50, 100);
        public static Guid[] CountryIds = ListCountries.Select(e => e.Id).ToArray();
        private static object _lock = new object();
        private static List<Country> _instance = null;

        public static List<Country> ListCountries
        {
            get
            {
                if (_lock == null)
                {
                    _lock = new object();
                }
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new List<Country>();
                        for (int i = 0; i < CountriesCnt; i++)
                        {
                            int contryNameLeng = DataCreateHelper.Random.Next(3, 18);
                            _instance.Add(new Country
                            {
                                Name = DataCreateHelper.RandomString(contryNameLeng)
                            });
                        }
                    }
                    return _instance;
                }
            }
        }
    }
}