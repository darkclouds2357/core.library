﻿using ST.Library.Base.Test.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ST.Library.Base.Test.Data
{
    public class CustomerData
    {
        public static IEnumerable<Customer> GenerateLargeListCustomer(int number)
        {
            for (int i = 0; i < number; i++)
            {
                int randomName = DataCreateHelper.Random.Next(3, 18);
                string customerEmail = DataCreateHelper.RandomEmail();
                int customerAge = DataCreateHelper.Random.Next(5, 80);
                yield return new Customer
                {
                    Name = DataCreateHelper.RandomString(randomName),
                    //Id = i + 1,
                    Age = customerAge,
                    Job = DataCreateHelper.RandomJob(),
                    Email = customerEmail
                };
            }
        }

        public static Customer GetRandomCustomer()
        {
            var cus = ListCustomers[DataCreateHelper.Random.Next(CustomersCnt)];
            while (cus == null)
            {
                cus = ListCustomers[DataCreateHelper.Random.Next(CustomersCnt)];
            }
            return cus;
        }

        public static IEnumerable<Customer> GetRandomCustomers(int cusNumber)
        {
            return Enumerable.Repeat(ListCustomers, cusNumber).Select(e => e[DataCreateHelper.Random.Next(e.Count)]);
        }

        public static readonly int CustomersCnt = DataCreateHelper.Random.Next(10000, 30000);
        private static object _lock = new object();
        private static List<Customer> _instance = null;

        public static List<Customer> ListCustomers
        {
            get
            {
                if (_lock == null) _lock = new object();
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new List<Customer>();
                        for (int i = 0; i < CustomersCnt; i++)
                        {
                            int customerNameLeng = DataCreateHelper.Random.Next(3, 18);
                            int customerAge = DataCreateHelper.Random.Next(5, 80);
                            Guid cityId = CityData.CityIds[DataCreateHelper.Random.Next(CityData.CitiesCnt)];

                            var currentVaildEmail = _instance.Select(e => e.Email);
                            string customerEmail = DataCreateHelper.RandomEmail();
                            while (currentVaildEmail.Contains(customerEmail))
                            {
                                customerEmail = DataCreateHelper.RandomEmail();
                            }
                            _instance.Add(new Customer
                            {
                                Name = DataCreateHelper.RandomString(customerNameLeng),
                                Age = customerAge,
                                Job = DataCreateHelper.RandomJob(),
                                Email = customerEmail
                            });
                        }
                    }
                    return _instance;
                }
            }
        }
    }
}