﻿using System;
using System.Linq;

namespace ST.Library.Base.Test.Data
{
    public class DataCreateHelper
    {
        public static Random Random = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";
        public const int MinPopulation = 5000;
        public const int MinAge = 18;

        private static readonly string[] Jobs = new string[] { "Dev", "SEO", "CEO", "Support", "Chelf", "Police", "Robber", "Gangster", "Asshole", "Heroin", "Bad-ass" };

        public static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[Random.Next(s.Length)]).ToArray()).Trim();
        }

        public static string RandomJob()
        {
            return Jobs[Random.Next(Jobs.Length - 1)];
        }

        public static string RandomEmail()
        {
            return ($"{RandomString(25).Trim()}@{RandomString(10).Trim()}.{RandomString(3).Trim()}").ToLower();
        }
    }
}