﻿using ST.Library.Base.Test.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ST.Library.Base.Test.Data
{
    public class UserData
    {
        public static readonly int UserCnt = DataCreateHelper.Random.Next(500, 1000);
        private static object _lock = new object();
        private static List<User> _instance = null;

        public static List<User> ListUsers
        {
            get
            {
                if (_lock == null) _lock = new object();
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new List<User>();
                        for (int i = 0; i < UserCnt; i++)
                        {
                            int userNameLeng = DataCreateHelper.Random.Next(3, 8);
                            int userAge = DataCreateHelper.Random.Next(5, 80);
                            bool isSystemUser = DataCreateHelper.Random.Next(0, 10) == 0;
                            var currentVaildEmail = _instance.Select(e => e.Email);
                            string userEmail = DataCreateHelper.RandomEmail();
                            while (currentVaildEmail.Contains(userEmail))
                            {
                                userEmail = DataCreateHelper.RandomEmail();
                            }
                            var firstName = DataCreateHelper.RandomString(DataCreateHelper.Random.Next(2, 6));
                            var lastName = DataCreateHelper.RandomString(DataCreateHelper.Random.Next(2, 6));

                            _instance.Add(new User
                            {
                                Email = userEmail,
                                IsSystemUser = isSystemUser,
                                UserName = DataCreateHelper.RandomString(userNameLeng),
                                FirstName = firstName,
                                LastName = lastName,
                                FullName = firstName + " " + lastName
                            });
                        }
                    }
                    return _instance;
                }
            }
        }
    }
}