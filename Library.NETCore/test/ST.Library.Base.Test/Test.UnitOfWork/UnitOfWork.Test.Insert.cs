﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using ST.Library.Base.Test.Data;
using ST.Library.Base.Test.DTO;
using ST.Library.Base.Test.Entities;
using ST.Library.Base.UnitOfWork;
using ST.Library.Common;
using ST.Library.Common.Caching;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ST.Library.Base.Test
{
    public class UnitOfWork_Insert_Test
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUser _currentUser;
        private readonly IDistributedCache _cache;
        private InMemoryContext db;

        //public UnitOfWork_Insert_Test()
        //{
        //    db = new InMemoryContext();
        //    //db.AddRange(UserData.ListUsers);
        //    //db.SaveChanges();

        //    //var user = db.Users.Find((long)DataCreateHelper.Random.Next(UserData.UserCnt));
        //    //while (user == null)
        //    //{
        //    //    user = db.Users.Find((long)DataCreateHelper.Random.Next(UserData.UserCnt));
        //    // }
        //    _currentUser = new CurrentUser
        //    {
        //        UserId = Guid.NewGuid()
        //    };

        //    IEnumerable<Type> repositoryTypes = new HashSet<Type>
        //    {
        //        typeof(Customer),
        //        typeof(User),
        //        typeof(Country)
        //    };
        //    this._unitOfWork = new UnitOfWork<InMemoryContext>(db, null, _currentUser, repositoryTypes);
        //    _cache = new MemoryDistributedCache(new MemoryCache(new MemoryCacheOptions()
        //    {
        //        ExpirationScanFrequency = new TimeSpan((DateTime.Now.AddHours(2).Ticks))
        //    }));
        //}

        //[Fact]
        //public void TestCache()
        //{
        //    var cities = CityData.GenerateLargeListCities(20).ToList();
        //    CacheOption.Instance.CacheTime = 3;
        //    var cacheCities = _cache.GetCache("list-city", () => { return cities; });
        //    Assert.Equal(cities.Count, cacheCities.Count);
        //    System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
        //    cacheCities = _cache.GetCache<List<City>>("list-city");
        //    Assert.Equal(cities.Count, cacheCities.Count);
        //    System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
        //    cacheCities = _cache.GetCache<List<City>>("list-city");
        //    Assert.Equal(cities.Count, cacheCities.Count);
        //    System.Threading.Thread.Sleep(TimeSpan.FromMinutes(2));
        //    cacheCities = _cache.GetCache<List<City>>("list-city");
        //    Assert.Equal(true, cacheCities == null);
        //}

        ////[Fact]
        //public void Insert_Entity()
        //{
        //    var customerRepository = _unitOfWork.GetRepository<Customer>();
        //    var cityRepository = _unitOfWork.GetRepository<City>();
        //    var countryRepository = _unitOfWork.GetRepository<Country>();

        //    var randomCountry = CountryData.GetRandomCountry();
        //    var randomCity = CityData.GetRandomCity();

        //    var randomCus = CustomerData.GetRandomCustomer();
        //    randomCity.Customers = new List<Customer> { randomCus };

        //    //customerRepository.Insert(randomCus);

        //    countryRepository.Insert(randomCountry);
        //    cityRepository.Insert(randomCity);
        //    var result = _unitOfWork.SaveChanges();
        //    Assert.Equal(true, result > 0);
        //}

        //[Fact]
        //public void Insert_List_Entity()
        //{
        //    int rowNumber = 5000000;
        //    var customerRepository = _unitOfWork.GetRepository<Customer>();
        //    var largeList = CustomerData.GenerateLargeListCustomer(rowNumber).ToList();
        //    Stopwatch st = Stopwatch.StartNew();
        //    customerRepository.Insert(largeList);
        //    var result = _unitOfWork.SaveChanges();
        //    var time = st.Elapsed.TotalSeconds;
        //    Assert.Equal(true, result >= 0);
        //}

        //[Fact]
        //public void Insert_List_EntityAsync()
        //{
        //    int rowNumber = 5000000;
        //    var customerRepository = _unitOfWork.GetRepository<Customer>();
        //    var countryRepository = _unitOfWork.GetRepository<Country>();
        //    var largeListCus = CustomerData.GenerateLargeListCustomer(rowNumber).ToList();
        //    var largeListCountry = CountryData.GenerateLargeListCountry(rowNumber).ToList();
        //    Stopwatch st = Stopwatch.StartNew();
        //    var insertCustomer = customerRepository.InsertAsync(largeListCus);
        //    var insertCity = countryRepository.InsertAsync(largeListCountry);
        //    //var result = _unitOfWork.SaveChanges();

        //    Task.WaitAll(insertCustomer, insertCity);
        //    var time = st.Elapsed.TotalSeconds;
        //    Assert.Equal(true, time >= 0);
        //}

        ////[Fact]
        //public void GetRepository()
        //{
        //    // Test SecuredCondition of Customer
        //    var repository = _unitOfWork.GetRepository<Customer>();
        //    var repSecuredCondition = repository?.SecuredCondition.ToString();
        //    Customer cus = new Customer();
        //    var cusSecuredCondition = cus.SecuredCondition.ToString();
        //    Assert.Equal(cusSecuredCondition, repSecuredCondition);

        //    // Test Set Tracking of Repository
        //    repository.Tracking = Enum.EFTracking.Tracking;
        //    var tracking = Enum.EFTracking.Tracking;

        //    Assert.Equal(tracking, repository?.Tracking);

        //    var rep = _unitOfWork.GetRepository<City>();
        //    Assert.Equal(true, rep != null);
        //}
    }
}